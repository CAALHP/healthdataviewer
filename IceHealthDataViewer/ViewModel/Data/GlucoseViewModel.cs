﻿using System;
using GalaSoft.MvvmLight;
using N4CLibrary;
using Net4Care.Observation;

namespace IceHealthDataViewer.ViewModel.Data
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class GlucoseViewModel : ViewModelBase, IEquatable<GlucoseViewModel>
    {
        private readonly StandardTeleObservation _sto;
        private readonly GlucoseObservation _glucose;

        /// <summary>
        /// Initializes a new instance of the Glucose class.
        /// </summary>
        public GlucoseViewModel(StandardTeleObservation sto)
        {
            _sto = sto;
            _glucose = _sto.ObservationSpecifics as GlucoseObservation;
        }

        public string Time
        {
            get
            {
                return TimeConverter.GetTime(_sto.Time).ToString();
            }
        }

        public DateTime TimeValue
        {
            get
            {
                return TimeConverter.GetTime(_sto.Time);
            }
        }

        public string Glucose
        {
            get
            {
                return _glucose != null ? _glucose.Glucose.Value + _glucose.Glucose.Unit : null;
            }
        }

        public double GlucoseValue
        {
            get
            {
                return _glucose.Glucose.Value;
            }
        }

        public bool Equals(GlucoseViewModel other)
        {
            return Glucose == other.Glucose && Time == other.Time;
        }
    }
}