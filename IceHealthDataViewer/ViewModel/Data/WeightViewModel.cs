﻿using System;
using GalaSoft.MvvmLight;
using N4CLibrary;
using Net4Care.Observation;

namespace IceHealthDataViewer.ViewModel.Data
{
    public class WeightViewModel : ViewModelBase, IEquatable<WeightViewModel>
    {
        private readonly WeightObservation _weight;
        private readonly StandardTeleObservation _sto;

        public WeightViewModel(StandardTeleObservation sto)
        {
            _sto = sto;
            _weight = _sto.ObservationSpecifics as WeightObservation;
        }

        public string Weight
        {
            get
            {
                return _weight != null ? _weight.Weight.Value + _weight.Weight.Unit : null;
            }
        }

        public double WeightValue
        {
            get
            {
                return _weight.Weight.Value;
            }
        }

        public string Time
        {
            get
            {
                return TimeConverter.GetTime(_sto.Time).ToString();
            }
        }

        public DateTime TimeValue
        {
            get
            {
                return TimeConverter.GetTime(_sto.Time);
            }
        }

        public string PatientId
        {
            get { return _sto.PatientCPR; }
        }

        public bool Equals(WeightViewModel other)
        {
            return Time == other.Time && Weight == other.Weight;
        }
    }
}
