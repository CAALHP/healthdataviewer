using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Windows;
using caalhp.Core.Events.Types;
using caalhp.Core.Utils.Helpers;
using caalhp.IcePluginAdapters;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using IceHealthDataViewer.Model;
using IceHealthDataViewer.Properties;
using IceHealthDataViewer.State;

namespace IceHealthDataViewer.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private BaseGridViewModel _currentViewModel;

        private BloodPressureGridViewModel _bloodPressureGridViewModel;
        private WeightGridViewModel _weightGridViewModel;
        private SaturationGridViewModel _saturationGridViewModel;
        private GlucoseGridViewModel _glucoseGridViewModel;
        private readonly IDataService _dataService;
        private List<UserProfile> _userList;
        private UserProfile _selectedUser;
        private bool _userListVisible;
        private HealthDataViewerImplementation _imp;
        private AppAdapter _adapter;
        private Dictionary<string, string> _textDictionary;

        public Dictionary<string, string> TextDictionary
        {
            get { return _textDictionary; }
            private set
            {
                _textDictionary = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel(IDataService dataService)
        {
            _dataService = dataService;

            InitCulture();

            //SetupTextFields();
            SetupTextDictionary();

            SetupViewModels();


            SetupRelayCommands();

            State = new UserLoggedOutState(this);

            UserListVisible = false;
            ConnectToCaalhp();

        }

        private void SetupRelayCommands()
        {
            BloodPressureCommand = new RelayCommand(BloodPressureCommand_Execute);
            WeightCommand = new RelayCommand(WeightCommand_Execute);
            SaturationCommand = new RelayCommand(SaturationCommand_Execute);
            GlucoseCommand = new RelayCommand(GlucoseCommand_Execute);

            YearCommand = new RelayCommand(YearCommand_Execute);
            MonthCommand = new RelayCommand(MonthCommand_Execute);
            WeekCommand = new RelayCommand(WeekCommand_Execute);
        }

        private void SetupViewModels()
        {
            _bloodPressureGridViewModel = new BloodPressureGridViewModel(_dataService);
            _weightGridViewModel = new WeightGridViewModel(_dataService);
            _saturationGridViewModel = new SaturationGridViewModel(_dataService);
            _glucoseGridViewModel = new GlucoseGridViewModel(_dataService);
            CurrentViewModel = _bloodPressureGridViewModel;
        }

        private void SetupTextDictionary()
        {
            TextDictionary = new Dictionary<string, string>
            {
                {"Weight", Resources.ResourceManager.GetString("Weight")},
                {"BloodPressure", Resources.ResourceManager.GetString("BloodPressure")},
                {"Saturation", Resources.ResourceManager.GetString("Saturation")},
                {"Glucose",Resources.ResourceManager.GetString("Glucose")},
                {"Year", Resources.ResourceManager.GetString("Year")},
                {"Month", Resources.ResourceManager.GetString("Month")},
                {"Week", Resources.ResourceManager.GetString("Week")}
            };
        }

        private static void InitCulture()
        {
            //get default culture
            var culture = CaalhpConfigHelper.GetDefaultCulture();
            if (String.IsNullOrWhiteSpace(culture))
                culture = "de-DE";
            //set language
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
        }

        private void ConnectToCaalhp()
        {
            const string endpoint = "localhost";
            try
            {
                _imp = new HealthDataViewerImplementation(this);
                _adapter = new AppAdapter(endpoint, _imp);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
        public void Show()
        {
            Messenger.Default.Send(new NotificationMessage("Show"));
        }

        public List<UserProfile> UserList
        {
            get { return _userList; }
            set
            {
                if (_userList == value) return;

                _userList = value;
                RaisePropertyChanged(() => UserList);
            }
        }

        public UserProfile SelectedUser
        {
            get { return _selectedUser; }
            set
            {
                if (_selectedUser == value) return;

                _selectedUser = value;
                RaisePropertyChanged(() => SelectedUser);

                if (_selectedUser != null)
                    Utility.Cpr = _selectedUser.UserId;
            }
        }

        public bool UserListVisible
        {
            get
            {
                return _userListVisible;
            }
            set
            {
                if (_userListVisible == value) return;
                _userListVisible = value;
                RaisePropertyChanged(() => UserListVisible);
            }
        }

        //public void UserLogIn()
        //{
        //    State.UserLoggedIn();
        //}

        //public void UserLogOut()
        //{
        //    State.UserLoggedOut();
        //}

        /// <summary>
        /// The CurrentView property.  The setter is private since only this 
        /// class can change the view via a command.  If the View is changed,
        /// we need to raise a property changed event (via INPC).
        /// </summary>
        public BaseGridViewModel CurrentViewModel
        {
            get
            {
                return _currentViewModel;
            }
            set
            {
                if (_currentViewModel == value)
                    return;
                _currentViewModel = value;
                //_currentViewModel.SortDescriptions.Add(new SortDescription("TimeValue", ListSortDirection.Descending));
                _currentViewModel.Sort();
                RaisePropertyChanged();
            }
        }

        public IHealthDateViewState State { get; set; }

        /// <summary>
        /// Simple property to hold the 'HealthDataViewCommand' - when executed
        /// it will change the current view to the 'HealthDataView'
        /// </summary>
        public RelayCommand BloodPressureCommand { get; private set; }
        public RelayCommand GlucoseCommand { get; private set; }
        public RelayCommand SaturationCommand { get; private set; }
        public RelayCommand WeightCommand { get; private set; }

        public RelayCommand YearCommand { get; private set; }
        public RelayCommand MonthCommand { get; private set; }
        public RelayCommand WeekCommand { get; private set; }

        private void BloodPressureCommand_Execute()
        {
            CurrentViewModel = _bloodPressureGridViewModel;
            // _bloodPressureGridViewModel.Sort();
        }

        private void GlucoseCommand_Execute()
        {
            CurrentViewModel = _glucoseGridViewModel;
        }

        private void SaturationCommand_Execute()
        {
            CurrentViewModel = _saturationGridViewModel;
        }

        private void WeightCommand_Execute()
        {
            CurrentViewModel = _weightGridViewModel;
        }

        private void YearCommand_Execute()
        {
            CurrentViewModel.SetYear();
        }

        private void MonthCommand_Execute()
        {
            CurrentViewModel.SetMonth();
        }

        private void WeekCommand_Execute()
        {
            CurrentViewModel.SetWeek();
        }
    }
}