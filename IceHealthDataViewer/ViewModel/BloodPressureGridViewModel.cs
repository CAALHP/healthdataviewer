﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using IceHealthDataViewer.Model;
using IceHealthDataViewer.Properties;
using IceHealthDataViewer.ViewModel.Data;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;

namespace IceHealthDataViewer.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class BloodPressureGridViewModel : BaseGridViewModel
    {
        private readonly IDataService _dataService;
        private ObservableCollection<HomeBloodPressureViewModel> _bloodPressures;
        private DispatcherTimer _updateDataTimer;
        private ICollectionView _bloodPressuresView;
        private PlotModel _bloodPressurePlotModel;
        private ScatterSeries _diastolicSeries;
        private ScatterSeries _systolicSeries;
        
        public ICollectionView BloodPressuresView
        {
            get { return _bloodPressuresView; }
            private set
            {
                _bloodPressuresView = value;
                RaisePropertyChanged();
            }
        }

        public ObservableCollection<HomeBloodPressureViewModel> BloodPressures
        {
            get { return _bloodPressures; }
            private set
            {
                _bloodPressures = value;
                RaisePropertyChanged();
            }
        }

        public PlotModel BloodPressurePlotModel
        {
            get { return _bloodPressurePlotModel; }
            private set
            {
                _bloodPressurePlotModel = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Initializes a new instance of the BloodPressureViewModel class.
        /// </summary>
        /// <param name="dataService"></param>
        public BloodPressureGridViewModel(IDataService dataService)
        {
            _dataService = dataService;
            BloodPressures = new ObservableCollection<HomeBloodPressureViewModel>();
            BloodPressuresView = CollectionViewSource.GetDefaultView(BloodPressures);
            BloodPressuresView.SortDescriptions.Add(new SortDescription("TimeValue", ListSortDirection.Descending));
            LoadDataCommand = new RelayCommand(LoadDataCommand_Execute);
            LoadDataCommand_Execute();
            SetupSeries();
            SetupPlot();
            SetupTimer();
            SetupTextDictionary();
        }

        private void SetupTextDictionary()
        {
            TextDictionary["BloodPressure"] = Resources.ResourceManager.GetString("BloodPressure");
            TextDictionary["Time"] = Resources.ResourceManager.GetString("Time");
            TextDictionary["Systolic"] = Resources.ResourceManager.GetString("Systolic");
            TextDictionary["Diastolic"] = Resources.ResourceManager.GetString("Diastolic");
            TextDictionary["Pulse"] = Resources.ResourceManager.GetString("Pulse");
        }

        private void SetupPlot()
        {
            var temp = new PlotModel("");
            //var ls = new ScatterSeries();
            //temp.Series.Add(ls);
            var dateAxis = new DateTimeAxis
            {
                Minimum = DateTimeAxis.ToDouble(DateTime.Now.Subtract(TimeSpan.FromDays(DaysToShow))),
                Maximum = DateTimeAxis.ToDouble(DateTime.Now)
            };
            temp.Axes.Add(dateAxis);
            var axis = new LinearAxis
            {
                Minimum = 0,
                Maximum = 300,
                //TextColor = OxyColors.Gold
                //Palette = 
                ShowMinorTicks = false,
            };

            temp.Axes.Add(axis);
            BloodPressurePlotModel = temp;

        }

        private void SetupSeries()
        {
            _diastolicSeries = new ScatterSeries { MarkerType = MarkerType.Square, MarkerSize = 6, MarkerStrokeThickness = 1.0, MarkerStroke = OxyColors.DarkBlue, MarkerFill = OxyColors.Blue };
            _systolicSeries = new ScatterSeries { MarkerType = MarkerType.Square, MarkerSize = 6, MarkerStrokeThickness = 1.0, MarkerStroke = OxyColors.DarkViolet, MarkerFill = OxyColors.Violet };
        }

        private void SetupTimer()
        {
            _updateDataTimer = new DispatcherTimer();
            _updateDataTimer.Tick += UpdateDataTimerOnTick;
            _updateDataTimer.Interval = new TimeSpan(0, 0, 10);
            _updateDataTimer.Start();
        }

        private void UpdateDataTimerOnTick(object sender, EventArgs eventArgs)
        {
            if (string.IsNullOrEmpty(Utility.Cpr))
                BloodPressures.Clear();
            else
                LoadDataCommand.Execute(this);
        }

        #region Command
        public ICommand LoadDataCommand { get; set; }

        private void LoadDataCommand_Execute()
        {
            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background, new Action(async () =>
            {
                try
                {
                    var bloodpressures = await _dataService.GetHomeBloodPressures();
                    foreach (var sto in bloodpressures)
                    {
                        var newVm = new HomeBloodPressureViewModel(sto);
                        if (!BloodPressures.Contains(newVm))
                        {
                            BloodPressures.Add(newVm);
                            UpdatePlotModel(newVm);
                        }
                    }
                    UpdateFilter();
                }
                catch (Exception e)
                {
                    //MessageBox.Show(e.Message);
                    Messenger.Default.Send(e);
                }
            }));
        }
        #endregion

        private void UpdatePlotModel(HomeBloodPressureViewModel hbp)
        {
            SetupPlot();
            //var ls = SaturationPlotModel.Series[0] as ScatterSeries;
            if (_diastolicSeries == null || _systolicSeries == null) return;

            _diastolicSeries.Points.Add(new ScatterPoint(DateTimeAxis.ToDouble(hbp.TimeValue), hbp.DiastolicValue));
            _systolicSeries.Points.Add(new ScatterPoint(DateTimeAxis.ToDouble(hbp.TimeValue), hbp.SystolicValue));

            BloodPressurePlotModel.Series.Add(_diastolicSeries);
            BloodPressurePlotModel.Series.Add(_systolicSeries);

            //SaturationPlotModel = temp;
            RaisePropertyChanged(() => BloodPressurePlotModel);
        }

        private void UpdateAxis()
        {
            if (BloodPressurePlotModel == null) return;
            if (BloodPressurePlotModel.Axes == null) return;
            var axis = BloodPressurePlotModel.Axes[0] as DateTimeAxis;
            if (axis == null) return;
            axis.Minimum = DateTimeAxis.ToDouble(DateTime.Now.Subtract(TimeSpan.FromDays(DaysToShow)));
            BloodPressurePlotModel.Axes[0] = axis;
        }

        protected override void DaysToShowUpdated()
        {
            UpdateAxis();
            UpdateFilter();
        }

        private void UpdateFilter()
        {
            var dayFilter =
                new Predicate<object>(
                    item => item != null && ((HomeBloodPressureViewModel)item).TimeValue >=
                            DateTime.Now.Subtract(TimeSpan.FromDays(DaysToShow)));
            if (BloodPressuresView != null && BloodPressuresView.CanFilter)
                BloodPressuresView.Filter = dayFilter;
        }

        public override void Sort()
        {
            BloodPressuresView.SortDescriptions.Add(new SortDescription("TimeValue", ListSortDirection.Descending));
        }
    }
}