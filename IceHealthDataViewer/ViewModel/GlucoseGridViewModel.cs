﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Data;
using System.Windows.Threading;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using IceHealthDataViewer.Model;
using IceHealthDataViewer.Properties;
using IceHealthDataViewer.ViewModel.Data;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;

namespace IceHealthDataViewer.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class GlucoseGridViewModel : BaseGridViewModel
    {
        private readonly IDataService _dataService;
        private DispatcherTimer _updateDataTimer;
        private ObservableCollection<GlucoseViewModel> _glucoses;
        private PlotModel _glucosePlotModel;
        private ScatterSeries _series;
        private ICollectionView _glucosesView;

        public ICollectionView GlucosesView
        {
            get { return _glucosesView; }
            private set
            {
                _glucosesView = value;
                RaisePropertyChanged();
            }
        }

        public ObservableCollection<GlucoseViewModel> Glucoses
        {
            get { return _glucoses; }
            private set
            {
                _glucoses = value;
                RaisePropertyChanged();
            }
        }

        public PlotModel GlucosePlotModel
        {
            get { return _glucosePlotModel; }
            private set
            {
                _glucosePlotModel = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Initializes a new instance of the GlucoseViewModel class.
        /// </summary>
        /// <param name="dataService"></param>
        public GlucoseGridViewModel(IDataService dataService)
        {
            _dataService = dataService;
            Glucoses = new ObservableCollection<GlucoseViewModel>();
            GlucosesView = CollectionViewSource.GetDefaultView(Glucoses);
            GlucosesView.SortDescriptions.Add(new SortDescription("TimeValue", ListSortDirection.Descending));
            LoadDataCommand = new RelayCommand(LoadDataCommand_Execute);
            LoadDataCommand_Execute();
            SetupSeries();
            SetupPlot();
            SetupTimer();
            SetupTextDictionary();
        }

        private void SetupTextDictionary()
        {
            TextDictionary["Glucose"] = Resources.ResourceManager.GetString("Glucose");
            TextDictionary["Time"] = Resources.ResourceManager.GetString("Time");
        }

        private void SetupSeries()
        {
            _series = new ScatterSeries { MarkerType = MarkerType.Square, MarkerSize = 6, MarkerStrokeThickness = 1.0, MarkerStroke = OxyColors.Black };
        }

        private void SetupPlot()
        {
            var temp = new PlotModel("");
            //var ls = new ScatterSeries();
            //temp.Series.Add(ls);
            var dateAxis = new DateTimeAxis
            {
                Minimum = DateTimeAxis.ToDouble(DateTime.Now.Subtract(TimeSpan.FromDays(DaysToShow))),
                Maximum = DateTimeAxis.ToDouble(DateTime.Now)
            };
            temp.Axes.Add(dateAxis);
            var axis = new LinearAxis
            {
                Minimum = 0,
                //Maximum = 100,
                //TextColor = OxyColors.Gold
                //Palette = 
                //ShowMinorTicks = false,
            };

            temp.Axes.Add(axis);
            GlucosePlotModel = temp;
        }
        private void SetupTimer()
        {
            _updateDataTimer = new DispatcherTimer();
            _updateDataTimer.Tick += UpdateDataTimerOnTick;
            _updateDataTimer.Interval = new TimeSpan(0, 0, 10);
            _updateDataTimer.Start();
        }

        private void UpdateDataTimerOnTick(object sender, EventArgs eventArgs)
        {
            if (string.IsNullOrEmpty(Utility.Cpr))
                Glucoses.Clear();
            else
                LoadDataCommand.Execute(this);
        }

        private void LoadDataCommand_Execute()
        {
            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background, new Action(async () =>
            {
                try
                {
                    var glucoses = await _dataService.GetGlucoses();
                    foreach (var glucose in glucoses)
                    {
                        var newGlucose = new GlucoseViewModel(glucose);
                        if (!Glucoses.Contains(newGlucose))
                        {
                            Glucoses.Add(newGlucose);
                            UpdatePlotModel(newGlucose);
                        }
                    }
                    UpdateFilter();
                }
                catch (Exception e)
                {
                    //MessageBox.Show(e.Message);
                    Messenger.Default.Send(e);
                }
            }));
        }

        public RelayCommand LoadDataCommand { get; set; }

        private void UpdatePlotModel(GlucoseViewModel glucose)
        {
            SetupPlot();
            //var ls = SaturationPlotModel.Series[0] as ScatterSeries;
            if (_series == null) return;

            _series.Points.Add(new ScatterPoint(DateTimeAxis.ToDouble(glucose.TimeValue), glucose.GlucoseValue));

            GlucosePlotModel.Series.Add(_series);

            //SaturationPlotModel = temp;
            RaisePropertyChanged(() => GlucosePlotModel);
        }

        private void UpdateAxis()
        {
            if (GlucosePlotModel != null)
            {
                if (GlucosePlotModel.Axes != null)
                {
                    var axis = GlucosePlotModel.Axes[0] as DateTimeAxis;
                    if (axis == null) return;
                    axis.Minimum = DateTimeAxis.ToDouble(DateTime.Now.Subtract(TimeSpan.FromDays(DaysToShow)));
                    GlucosePlotModel.Axes[0] = axis;
                }
            }
        }

        protected override void DaysToShowUpdated()
        {
            UpdateAxis();
            UpdateFilter();
        }

        private void UpdateFilter()
        {
            var dayFilter =
                new Predicate<object>(
                    item => item != null && ((GlucoseViewModel)item).TimeValue >=
                            DateTime.Now.Subtract(TimeSpan.FromDays(DaysToShow)));
            if (GlucosesView != null && GlucosesView.CanFilter)
                GlucosesView.Filter = dayFilter;
        }

        public override void Sort()
        {
            GlucosesView.SortDescriptions.Add(new SortDescription("TimeValue", ListSortDirection.Descending));
        }
    }
}