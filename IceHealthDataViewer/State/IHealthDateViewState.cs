﻿namespace IceHealthDataViewer.State
{
    public interface IHealthDateViewState
    {
        void UserLoggedIn();
        void UserLoggedOut();
        void Show();
    }
}
