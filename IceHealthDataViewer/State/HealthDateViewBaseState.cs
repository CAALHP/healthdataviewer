﻿using IceHealthDataViewer.ViewModel;

namespace IceHealthDataViewer.State
{
    public abstract class HealthDateViewBaseState: IHealthDateViewState
    {
        protected readonly MainViewModel MainView;

        protected HealthDateViewBaseState(MainViewModel mainView)
        {
            MainView = mainView;
        }

        public abstract void UserLoggedIn();
        public abstract void UserLoggedOut();
        public abstract void Show();
    }
}
