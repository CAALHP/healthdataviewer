﻿using IceHealthDataViewer.ViewModel;

namespace IceHealthDataViewer.State
{
    public class UserLoggedOutState: HealthDateViewBaseState
    {
        public UserLoggedOutState(MainViewModel mainView) : base(mainView)
        {
        }

        public override void UserLoggedIn()
        {
            MainView.State = new UserLoggedInState(MainView);
            //Messenger.Default.Send(new NotificationMessage("Hide"));
        }

        public override void UserLoggedOut()
        {
            //empty function by statepattern design
        }

        public override void Show()
        {
            //Messenger.Default.Send(new NotificationMessage(this,"Hide"));
            //empty function by statepattern design
        }
    }
}
