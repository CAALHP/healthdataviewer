﻿using GalaSoft.MvvmLight.Messaging;
using IceHealthDataViewer.ViewModel;

namespace IceHealthDataViewer.State
{
    public class UserLoggedInState: HealthDateViewBaseState
    {
        public UserLoggedInState(MainViewModel mainView) : base(mainView)
        {
            
        }

        public override void UserLoggedIn()
        {
            //empty function by statepattern design
        }

        public override void UserLoggedOut()
        {
            MainView.State = new UserLoggedOutState(MainView);
            Messenger.Default.Send(new NotificationMessage("Hide"));
        }

        public override void Show()
        {
            Messenger.Default.Send(new NotificationMessage("Show"));
        }
    }
}
