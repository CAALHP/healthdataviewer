﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using IceHealthDataViewer.Model;
using N4CLibrary;
using Net4Care.Observation;

namespace IceHealthDataViewer.Design
{
    public class DesignDataService : IDataService
    {
        public IList<StandardTeleObservation> GetHomeBloodPressures()
        {
            var result = new List<StandardTeleObservation>();
            for (var i = 0; i < 10; i++)
            {
                /*result.Add(
                    new StandardTeleObservation());
                        (
                        sys: 100+i*10, 
                        dia: 60+i*5, 
                        pulse: 40+i*10, 
                        noise: i%2==0, 
                        timeSeated: i*10
                        ));*/
            }
            return result;
        }

        public Task<IList<StandardTeleObservation>> GetWeights()
        {
            throw new NotImplementedException();
        }

        Task IDataService.UploadHomeBloodPressure(StandardTeleObservation homeBloodPressure)
        {
            throw new NotImplementedException();
        }

        Task<IList<StandardTeleObservation>> IDataService.GetSaturations()
        {
            throw new NotImplementedException();
        }

        Task<IList<StandardTeleObservation>> IDataService.GetGlucoses()
        {
            throw new NotImplementedException();
        }

        public IList<StandardTeleObservation> GetHomeWeights()
        {
            throw new NotImplementedException();
        }

        Task<IList<StandardTeleObservation>> IDataService.GetHomeBloodPressures()
        {
            return Task.Run(() =>
            {
                IList<StandardTeleObservation> result = new List<StandardTeleObservation>();
                /*for (var i = 0; i < 10; i++)
                {
                    var sto = new StandardTeleObservation(,);
                    sto.ObservationSpecifics = new HomeBloodPressureObservation(
                            sys: 100 + i * 10,
                            dia: 60 + i * 5,
                            pulse: 40 + i * 10,
                            noise: i % 2 == 0,
                            timeSeated: i * 10
                        );
                    result.Add(
                        new StandardTeleObservation()
                            (
                            ));
                }*/
                return result;    
            });
            
        }

        public void UploadHomeBloodPressure(StandardTeleObservation homeBloodPressure)
        {
            throw new NotImplementedException();
        }

        public IList<StandardTeleObservation> GetSaturations()
        {
            throw new NotImplementedException();
        }

        public IList<StandardTeleObservation> GetGlucoses()
        {
            throw new NotImplementedException();
        }

        public void InitCollections()
        {
            throw new NotImplementedException();
        }

        public void UploadHomeBloodPressure(HomeBloodPressureObservation homeBloodPressureObservation)
        {
            throw new NotImplementedException();
        }
    }
}