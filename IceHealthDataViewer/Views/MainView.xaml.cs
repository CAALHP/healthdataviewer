﻿using System.Windows;
using GalaSoft.MvvmLight.Messaging;
using GalaSoft.MvvmLight.Threading;

namespace IceHealthDataViewer.Views
{
    /// <summary>
    /// Description for MainView.
    /// </summary>
    public partial class MainView : Window
    {
        /// <summary>
        /// Initializes a new instance of the MainView class.
        /// </summary>
        public MainView()
        {
            InitializeComponent();
            
            Messenger.Default.Register<NotificationMessage>(this, ShowView);
        }

        private void ShowView(NotificationMessage note)
        {
            if (note.Notification == "Show")
            {
                DispatcherHelper.CheckBeginInvokeOnUI(Show);
                DispatcherHelper.CheckBeginInvokeOnUI(BringToFront);
            }
            else if (note.Notification == "Hide")
            {
                DispatcherHelper.CheckBeginInvokeOnUI(Hide);
            }
        }

        private void BringToFront()
        {
            Activate();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            Hide();
        }
    }
}