﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace IceHealthDataViewer.Views
{
    /// <summary>
    /// Description for BloodPressureView.
    /// </summary>
    public partial class BloodPressureView
    {
        /// <summary>
        /// Initializes a new instance of the BloodPressureView class.
        /// </summary>
        public BloodPressureView()
        {
            InitializeComponent();

            //oxyplot won't update by itself unless we do this:
            var dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Tick += DispatcherTimerOnTick;
            dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 50);
            dispatcherTimer.Start();
        }

        private void DispatcherTimerOnTick(object sender, EventArgs eventArgs)
        {
            Plot.InvalidatePlot();
        }

        private void BloodPressureDataGrid_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            BloodPressureDataGrid.UnselectAllCells();
        }

        private void ScrollUpButton_Click(object sender, RoutedEventArgs e)
        {
            var row = UiHelper.GetControl<DataGridRow>(BloodPressureDataGrid);
            var scroller = UiHelper.GetControl<ScrollViewer>(BloodPressureDataGrid);
            if (row != null) scroller.ScrollToVerticalOffset(scroller.VerticalOffset - row.ActualHeight);
        }

        private void ScrollDownButton_Click(object sender, RoutedEventArgs e)
        {
            var row = UiHelper.GetControl<DataGridRow>(BloodPressureDataGrid);
            var scroller = UiHelper.GetControl<ScrollViewer>(BloodPressureDataGrid);
            if (row != null) scroller.ScrollToVerticalOffset(scroller.VerticalOffset + row.ActualHeight);
        }
    }
}