﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace IceHealthDataViewer.Views
{
    /// <summary>
    /// Description for WeightView.
    /// </summary>
    public partial class WeightView
    {
        /// <summary>
        /// Initializes a new instance of the WeightView class.
        /// </summary>
        public WeightView()
        {
            InitializeComponent();

            //oxyplot won't update by itself unless we do this:
            var dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Tick += DispatcherTimerOnTick;
            dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 50);
            dispatcherTimer.Start();
        }

        private void DispatcherTimerOnTick(object sender, EventArgs eventArgs)
        {
            Plot.InvalidatePlot();
        }

        private void WeightDataGrid_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            WeightDataGrid.UnselectAllCells();
        }

        private void ScrollUpButton_Click(object sender, RoutedEventArgs e)
        {
            var row = UiHelper.GetControl<DataGridRow>(WeightDataGrid);
            var scroller = UiHelper.GetControl<ScrollViewer>(WeightDataGrid);
            if (row != null) scroller.ScrollToVerticalOffset(scroller.VerticalOffset - row.ActualHeight);
        }

        private void ScrollDownButton_Click(object sender, RoutedEventArgs e)
        {
            var row = UiHelper.GetControl<DataGridRow>(WeightDataGrid);
            var scroller = UiHelper.GetControl<ScrollViewer>(WeightDataGrid);
            if (row != null) scroller.ScrollToVerticalOffset(scroller.VerticalOffset + row.ActualHeight);
        }
    }
}