﻿using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;
using IceHealthDataViewer.OpenHealthService;
using Net4Care.Observation;
using N4CLibrary;
using PHEI.DTO;
using System;
using System.ServiceModel;
using System.Diagnostics;

namespace IceHealthDataViewer.Model
{
    public class DataServiceOpenHealth : IDataService
    {
        //private readonly N4CHelper _n4CHelperBlood;
        //private readonly N4CHelper _n4CHelperSaturation;
        //private readonly N4CHelper _n4CHelperGlucose;
        //private readonly N4CHelper _n4CHelperWeight;
        //private readonly string _cpr;
        private PatientServiceClient service;
        private string _cpr;
        private string _passkey;

        private static DateTime Jan1st1970 = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        
        public long getCurrentTimeInMillis(DateTime time)
        {
            return (long)((time.ToUniversalTime() - Jan1st1970).TotalMilliseconds);
        }

        public DataServiceOpenHealth()
        {


            ConnectToPHRService();

            HandlePatientData();
            

        }

        private void ConnectToPHRService()
        {
            try
            {
                var address = ConfigurationManager.AppSettings.Get("OpenHealthPHRserveraddress");


                BasicHttpBinding httpBinding = new BasicHttpBinding();
                httpBinding.MaxReceivedMessageSize = 2147483647;
                httpBinding.MaxBufferSize = 2147483647;


                EndpointAddress endpoint = new EndpointAddress("http://localhost:8083/VitalSignsService");
                //System.Diagnostics.Debugger.Launch();
                //TODO: refactor to dynamic adress

                service = new PatientServiceClient(httpBinding, endpoint);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error while connecting to OpenHealthPHR sevic");
            }
        }

        private void HandlePatientData()
        {
            if (service == null) return;

            try
            {
                Patient[] patients = service.RetrieveAllUnassignedPatients();
                //Patient[] patients = imp.RetrieveAllPatients();

                if (patients == null || patients.Length == 0)
                {
                    CurrentPatient = null;
                    var dto = new Patient();
                    dto.HealthCareUserID = 0;
                    dto.SocialSecurity = "Test";
                    dto.PassKey = "Test";

                    CurrentPatient = service.CreateNewPatient(dto);

                }
                else
                {
                    CurrentPatient = patients[0];

                }
                _cpr = CurrentPatient.SocialSecurity;
                _passkey = CurrentPatient.PassKey;
                var result = service.RetrieveAllVitalSigns(_cpr, _passkey);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error during handling data: " + e);
            }
        }

        private void ConnectToService()
        {
            try
            {
                var address = ConfigurationManager.AppSettings.Get("OpenHealthPHRserveraddress");


                BasicHttpBinding httpBinding = new BasicHttpBinding();
                httpBinding.MaxReceivedMessageSize = 2147483647;
                httpBinding.MaxBufferSize = 2147483647;


                EndpointAddress endpoint = new EndpointAddress("http://localhost:8083/VitalSignsService");
                //System.Diagnostics.Debugger.Launch();
                //TODO: refactor to dynamic adress

                service = new PatientServiceClient(httpBinding, endpoint);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error while connecting to OpenHealthPHR sevic");
            }
        }

        //public void InitCollections()
        //{
        //    var minutes = long.Parse(ConfigurationManager.AppSettings.Get("minutestolookback"));
        //    _bloodCollection = new ObservableCollection<StandardTeleObservation>(_n4CHelperBlood.GetFilteredObservations<HomeBloodPressureObservation>(_cpr, minutes));
        //    _saturationCollection = new ObservableCollection<StandardTeleObservation>(_n4CHelperSaturation.GetFilteredObservations<SaturationObservation>(_cpr, minutes));
        //    _glucoseCollection = new ObservableCollection<StandardTeleObservation>(_n4CHelperGlucose.GetFilteredObservations<GlucoseObservation>(_cpr, minutes));
        //    _weightCollection = new ObservableCollection<StandardTeleObservation>(_n4CHelperWeight.GetFilteredObservations<WeightObservation>(_cpr, minutes));
        //}

     
        public async Task<IList<StandardTeleObservation>> GetHomeBloodPressures()
        {
            
            return await Task.Run(() =>
            {
                var list = new List<StandardTeleObservation>();
             
                try {
                var minutes = long.Parse(ConfigurationManager.AppSettings.Get("minutestolookback"));
                //return _n4CHelperWeight.GetFilteredObservations<WeightObservation>(Utility.Cpr, minutes);
                VitalSign[] vss = service.RetrieveAllVitalSigns(_cpr, _passkey);
                foreach (var vs in vss)
                {
                    if ((vs is BloodPressure) && (vs.TimeStamp > DateTime.Now.AddMinutes(0 - minutes)))
                    {
                        var systolic = ((BloodPressure)vs).Systolic;
                        var diastolic = ((BloodPressure)vs).Diastolic;
                        var pulse = ((BloodPressure)vs).HeartRate;
                        var item = new HomeBloodPressureObservation(systolic, diastolic, pulse, null);
                        
                        var obs = new StandardTeleObservation(_cpr, "", "", "", null, item, "");
                        obs.Time = getCurrentTimeInMillis(vs.TimeStamp.ToUniversalTime());

                        list.Add(obs);
                    }
         
                }
                }
                catch (Exception e)
                {
                    Console.WriteLine();
                }
                return list;

            });
        }

        public async Task<IList<StandardTeleObservation>> GetWeights()
        {
            return await Task.Run(() =>
            {

                var list = new List<StandardTeleObservation>();
             
                try {

                var minutes = long.Parse(ConfigurationManager.AppSettings.Get("minutestolookback"));
                //return _n4CHelperWeight.GetFilteredObservations<WeightObservation>(Utility.Cpr, minutes);
                VitalSign[] vss = service.RetrieveAllVitalSigns(_cpr, _passkey);
                 foreach (var vs in vss)
                 {
                     if ((vs is Weight) && (vs.TimeStamp > DateTime.Now.AddMinutes(0-minutes))) 
                     {
                         double weight = Math.Round(((Weight)vs).WeigthInGrams/1000.0, 1);
                         var item = new WeightObservation(weight, 100, new Net4CareContext(Net4CareContext.AuthorType.SELF, Net4CareContext.ProvisionType.ELECTRONIC));
                         var obs = new StandardTeleObservation(_cpr, "", "", "", null, item, "");
                         obs.Time = getCurrentTimeInMillis(vs.TimeStamp.ToUniversalTime());
                         
                         list.Add(obs);
                     }
                 }
                }
                catch (Exception e)
                {
                    Console.WriteLine();
                }

                    return list;


            });
        }

        public async Task UploadHomeBloodPressure(StandardTeleObservation homeBloodPressure)
        {
            //await Task.Run(() => _n4CHelperBlood.UploadBloodPressureObservation(Utility.Cpr, homeBloodPressure.ObservationSpecifics));
        }

        public async Task<IList<StandardTeleObservation>> GetSaturations()
        {
            return await Task.Run(() =>
            {
                var list = new List<StandardTeleObservation>();

                try
                {

                var minutes = long.Parse(ConfigurationManager.AppSettings.Get("minutestolookback"));
                //return _n4CHelperWeight.GetFilteredObservations<WeightObservation>(Utility.Cpr, minutes);
                VitalSign[] vss = service.RetrieveAllVitalSigns(_cpr, _passkey);
                foreach (var vs in vss)
                {
                    if ((vs is Oximeter) && (vs.TimeStamp > DateTime.Now.AddMinutes(0 - minutes)))
                    {
                        var sat = ((Oximeter)vs).SaturationLevel;
                        var item = new SaturationObservation(sat);
                        var obs = new StandardTeleObservation(_cpr, "", "", "", null, item, "");
                        obs.Time = getCurrentTimeInMillis(vs.TimeStamp.ToUniversalTime());

                        list.Add(obs);
                    }
                }
                 }
                catch (Exception e)
                {
                    Console.WriteLine();
                }

                return list;


            });
        }

        public async Task<IList<StandardTeleObservation>> GetGlucoses()
        {
            return await Task.Run(() =>
            {
                var list = new List<StandardTeleObservation>();
             
                try {

                var minutes = long.Parse(ConfigurationManager.AppSettings.Get("minutestolookback"));
                //return _n4CHelperWeight.GetFilteredObservations<WeightObservation>(Utility.Cpr, minutes);
                VitalSign[] vss = service.RetrieveAllVitalSigns(_cpr, _passkey);
                foreach (var vs in vss)
                {
                    if ((vs is BloodSugar) && (vs.TimeStamp > DateTime.Now.AddMinutes(0 - minutes)))
                    {
                        var bs = ((BloodSugar)vs).BloodSugarLevel;
                        var item = new GlucoseObservation(bs);
                        var obs = new StandardTeleObservation(_cpr, "", "", "", null, item, "");
                        obs.Time = getCurrentTimeInMillis(vs.TimeStamp.ToUniversalTime());

                        list.Add(obs);
                    }
                }
                }
                catch (Exception e)
                {
                    Console.WriteLine();
                }

                    return list;


            });

        }

        public async Task UploadHomeBloodPressure(HomeBloodPressureObservation homeBloodPressureObservation)
        {
            //await Task.Run(() => _n4CHelperBlood.UploadBloodPressureObservation(Utility.Cpr, homeBloodPressureObservation));
        }

        public Patient CurrentPatient { get; set; }
    }
}