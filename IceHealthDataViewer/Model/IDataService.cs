﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Net4Care.Observation;

namespace IceHealthDataViewer.Model
{
    public interface IDataService
    {
        Task<IList<StandardTeleObservation>> GetHomeBloodPressures();
        Task<IList<StandardTeleObservation>> GetWeights();
        Task UploadHomeBloodPressure(StandardTeleObservation homeBloodPressure);
        Task<IList<StandardTeleObservation>> GetSaturations();
        Task<IList<StandardTeleObservation>> GetGlucoses();
        //void InitCollections();
    }
}
