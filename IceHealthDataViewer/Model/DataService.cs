﻿using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;
using N4CLibrary;
using Net4Care.Observation;

namespace IceHealthDataViewer.Model
{
    public class DataService : IDataService
    {
        private readonly N4CHelper _n4CHelperBlood;
        private readonly N4CHelper _n4CHelperSaturation;
        private readonly N4CHelper _n4CHelperGlucose;
        private readonly N4CHelper _n4CHelperWeight;
        //private readonly string _cpr;
        //private ObservableCollection<StandardTeleObservation> _bloodCollection;
        //private ObservableCollection<StandardTeleObservation> _saturationCollection;
        //private ObservableCollection<StandardTeleObservation> _glucoseCollection;
        //private ObservableCollection<StandardTeleObservation> _weightCollection;

        public DataService()
        {
            var address = ConfigurationManager.AppSettings.Get("serveraddress");
            _n4CHelperBlood = new N4CHelper(address, typeof(HomeBloodPressureObservation));
            _n4CHelperSaturation = new N4CHelper(address, typeof(SaturationObservation));
            _n4CHelperGlucose = new N4CHelper(address, typeof(GlucoseObservation));
            _n4CHelperWeight = new N4CHelper(address, typeof(WeightObservation));
            //_cpr = ConfigurationManager.AppSettings.Get("cpr");

            //_bloodCollection = new ObservableCollection<StandardTeleObservation>();
            //_saturationCollection = new ObservableCollection<StandardTeleObservation>();
            //_glucoseCollection = new ObservableCollection<StandardTeleObservation>();
            //_weightCollection = new ObservableCollection<StandardTeleObservation>();
        }

        //public void InitCollections()
        //{
        //    var minutes = long.Parse(ConfigurationManager.AppSettings.Get("minutestolookback"));
        //    _bloodCollection = new ObservableCollection<StandardTeleObservation>(_n4CHelperBlood.GetFilteredObservations<HomeBloodPressureObservation>(_cpr, minutes));
        //    _saturationCollection = new ObservableCollection<StandardTeleObservation>(_n4CHelperSaturation.GetFilteredObservations<SaturationObservation>(_cpr, minutes));
        //    _glucoseCollection = new ObservableCollection<StandardTeleObservation>(_n4CHelperGlucose.GetFilteredObservations<GlucoseObservation>(_cpr, minutes));
        //    _weightCollection = new ObservableCollection<StandardTeleObservation>(_n4CHelperWeight.GetFilteredObservations<WeightObservation>(_cpr, minutes));
        //}

        public async Task<IList<StandardTeleObservation>> GetHomeBloodPressures()
        {
            return await Task.Run(() =>
            {
                var minutes = long.Parse(ConfigurationManager.AppSettings.Get("minutestolookback"));
                return _n4CHelperBlood.GetFilteredObservations<HomeBloodPressureObservation>(Utility.Cpr, minutes);
            });
        }

        public async Task<IList<StandardTeleObservation>> GetWeights()
        {
            return await Task.Run(() =>
            {
                var minutes = long.Parse(ConfigurationManager.AppSettings.Get("minutestolookback"));
                return _n4CHelperWeight.GetFilteredObservations<WeightObservation>(Utility.Cpr, minutes);
            });
        }

        public async Task UploadHomeBloodPressure(StandardTeleObservation homeBloodPressure)
        {
            await Task.Run(() => _n4CHelperBlood.UploadBloodPressureObservation(Utility.Cpr, homeBloodPressure.ObservationSpecifics));
        }

        public async Task<IList<StandardTeleObservation>> GetSaturations()
        {
            return await Task.Run(() =>
            {
                var minutes = long.Parse(ConfigurationManager.AppSettings.Get("minutestolookback"));
                return _n4CHelperSaturation.GetFilteredObservations<SaturationObservation>(Utility.Cpr, minutes);
            });
        }

        public async Task<IList<StandardTeleObservation>> GetGlucoses()
        {
            return await Task.Run(() =>
            {
                var minutes = long.Parse(ConfigurationManager.AppSettings.Get("minutestolookback"));
                return _n4CHelperGlucose.GetFilteredObservations<GlucoseObservation>(Utility.Cpr, minutes);
            });

        }

        public async Task UploadHomeBloodPressure(HomeBloodPressureObservation homeBloodPressureObservation)
        {
            await Task.Run(() => _n4CHelperBlood.UploadBloodPressureObservation(Utility.Cpr, homeBloodPressureObservation));
        }
    }
}