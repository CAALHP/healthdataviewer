﻿namespace IceHealthDataViewer.Model
{
    public static class Utility
    {
        private static readonly object _lock = new object();

        private static string _cpr;
        public static string Cpr
        {
            get
            {
                string res;
                lock (_lock)
                {
                    res = _cpr;
                }
                return res;
            }
            set
            {
                lock (_lock)
                {
                    _cpr = value;
                }
            }
        }
    }
}
