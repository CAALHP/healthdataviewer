﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Windows;
using System.Windows.Threading;
using caalhp.Core.Contracts;
using caalhp.Core.Events;
using caalhp.Core.Events.UserServiceEvents;
using caalhp.Core.Utils.Helpers;
using caalhp.Core.Utils.Helpers.Serialization;
using caalhp.Core.Utils.Helpers.Serialization.JSON;
using GalaSoft.MvvmLight.Messaging;
using IceHealthDataViewer.Model;
using IceHealthDataViewer.ViewModel;

namespace IceHealthDataViewer
{
    public class HealthDataViewerImplementation : IAppCAALHPContract
    {
        private IAppHostCAALHPContract _host;
        private int _processId;
        private readonly MainViewModel _main;

        public HealthDataViewerImplementation(MainViewModel mainViewModel)
        {
            try
            {
                //var vml = Application.Current.Resources["Locator"] as ViewModelLocator;
                //if (vml == null) return;
                _main = mainViewModel;

                Utility.Cpr = ConfigurationManager.AppSettings.Get("cpr");
            }
            catch (Exception e)
            {
                //MessageBox.Show(e.Message);
                Console.WriteLine("Failed creating the HealthDataViewerImplementation " + e);
            }
        }


        public string GetName()
        {
            return "HealthDataViewer";
        }

        public bool IsAlive()
        {
            return true;
        }

        public void ShutDown()
        {
            Application.Current.Dispatcher.BeginInvokeShutdown(DispatcherPriority.Normal);
        }

        public void Initialize(IAppHostCAALHPContract hostObj, int processId)
        {
            _host = hostObj;
            _processId = processId;
            //Subscribe to events so we can react to "Home" messages
            //_host.Host.SubscribeToEvents(_processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(ShowAppEvent)), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(UserListResponseEvent)), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(UserLoggedInEvent)), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(UserLoggedOutEvent)), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(IsUserLoggedInResponsEvent)), _processId);

            //Handle exception messages from sub views
            Messenger.Default.Register<Exception>(this, ReportError);

            RequestIsUserLogged();
        }

        private void ReportError(Exception exception)
        {
            var error = new ErrorEvent {ErrorMessage = exception.Message};
            _host.Host.ReportEvent(EventHelper.CreateEvent(SerializationType.Json, error));
        }

        public void Show()
        {
            DispatchToApp(caalhp.IcePluginAdapters.WPF.Helper.BringToFront);
        }

        public IList<IPluginInfo> GetListOfInstalledApps()
        {
            throw new NotImplementedException();
        }

        public void Notify(KeyValuePair<string, string> notification)
        {
            var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key);
            dynamic obj = JsonSerializer.DeserializeEvent(notification.Value, type);
            HandleEvent(obj);
        }



        private void HandleEvent(ShowAppEvent e)
        {
            if (e.AppName.Equals("HealthDataViewer"))
            {
                //Show homescreen
                //Show();
                RequestIsUserLogged();
            }
        }

        private void HandleEvent(UserListResponseEvent e)
        {
            _main.UserList = e.Users;
        }

        private void HandleEvent(UserLoggedInEvent obj)
        {
            Utility.Cpr = obj.User.UserId;
            _main.UserListVisible = true;

            RequestUserListFromCaalhp();
        }

        private void HandleEvent(UserLoggedOutEvent obj)
        {
            _main.UserListVisible = false;
            _main.SelectedUser = null;

            Utility.Cpr = ConfigurationManager.AppSettings.Get("cpr");
        }

        private void HandleEvent(IsUserLoggedInResponsEvent e)
        {
            if (e.User != null)
            {
                Utility.Cpr = e.User.UserId;
                _main.UserListVisible = true;
            }
            Show();
        }

        private void RequestIsUserLogged()
        {
            var req = new IsUserLoggedInRequestEvent()
                {
                    CallerProcessId = _processId,
                    CallerName = GetName()
                };

            var serializedResponse = EventHelper.CreateEvent(SerializationType.Json, req);
            _host.Host.ReportEvent(serializedResponse);
        }

        private void RequestUserListFromCaalhp()
        {
            var reqEvent = new UserListRequestEvent()
            {
                CallerName = GetName(),
                CallerProcessId = _processId
            };

            var serializedEvent = EventHelper.CreateEvent(SerializationType.Json, reqEvent);
            _host.Host.ReportEvent(serializedEvent);
        }

        private void DispatchToApp(Action action)
        {
            Application.Current.Dispatcher.Invoke(action);
        }

    }
}